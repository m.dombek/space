bullets = {}

function bullets_spawn(x, y)
  local bullet = {}

  bullet.x = x
  bullet.y = y
  bullet.speed = 500
  bullet.direction = player_angle()
  bullet.dead = false

  table.insert(bullets, bullet)
end

function bullets_update(dt)
  for i, bullet in ipairs(bullets) do
    bullet.x = bullet.x - math.cos(bullet.direction) * bullet.speed * dt
    bullet.y = bullet.y - math.sin(bullet.direction) * bullet.speed * dt
  end

  for i = #bullets, 1, -1 do
    local b = bullets[i]
    if b.dead or distanceBetween(b.x, b.y, player.x, player.y) > 500 then
      table.remove(bullets, i)
    end
  end
end

function bullets_draw()
  for i, bullet in ipairs(bullets) do
    love.graphics.circle("fill", bullet.x, bullet.y, 20)
  end
end
