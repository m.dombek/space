player = {}
player.x = love.graphics:getWidth() / 2
player.y = love.graphics:getHeight() / 2 + shiftDown
player.speed = 100
player.grid = anim8.newGrid(46, 46, 92, 46)
player.animation = anim8.newAnimation(player.grid('1-2', 1), 0.1)

function player_angle()
  return math.atan2(love.graphics:getHeight() / 2 - prevTap.y, love.graphics:getWidth() / 2 - prevTap.x)
end

function player_update(dt)
  player.x = player.x - math.cos(player_angle()) * player.speed * dt
  player.y = player.y - math.sin(player_angle()) * player.speed * dt
end
