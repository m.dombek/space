function love.conf(t)
	t.modules.joystick = false --Decreases start up time
	t.externalstorage = true
	t.window.width = 720
	t.window.height = 960
end