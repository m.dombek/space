function love.load()
  setScreenSize()

  anim8 = require('libraries/anim8-master/anim8')
  cameraFile = require('libraries/hump-master/camera')
  require('classes/player')
  require('classes/bullet')

  sprites = {}
  sprites.playerSheet = love.graphics.newImage("sprites/ship_spritesheet.png")
  sprites.background = love.graphics.newImage("sprites/background.png")

  prevTap = {}
  prevTap.x = love.graphics:getWidth() / 2
  prevTap.y = 0

  camera = cameraFile()
end

function love.update(dt)
  player.animation:update(dt)
  player_update(dt)
  bullets_update(dt)

  camera:lookAt(player.x, player.y)
end

function love.draw()
  camera:attach()

  love.graphics.draw(sprites.background, 0, 0, nil, 1.5, 1.5)
  player.animation:draw(sprites.playerSheet, player.x, player.y, player_angle() + 3 * math.pi / 2, 1.5, 1.5, 23, 23)
  bullets_draw()

  camera:detach()
end

function love.mousepressed(x, y)
  prevTap.x = x
  prevTap.y = y
  bullets_spawn(player.x, player.y)
end

function setScreenSize()
  coreWidth = 720
  coreHeight = 960
  scale = 1
  shiftDown = 0
  osString = love.system.getOS()

  if osString == "Android" or osString == "iOS" then
    scale = love.graphics.getWidth() / coreWidth
    shiftDown = (love.graphics.getHeight() - (coreHeight * scale)) / 2 / scale
  else
    scale = 0.6
  end

  love.window.setMode(coreWidth * scale, coreHeight * scale)
end

function distanceBetween(x1, y1, x2, y2)
  return math.sqrt(math.pow((y2 - y1), 2) + math.pow((x2 - x1), 2))
end
